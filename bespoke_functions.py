import pandas as pd
import datetime as dt

def create_date_components(dataframe):
    
    """"random forests can't handle datetimes, so we'll pull out the aspects of date and time to new columns"""
    
    func_df = dataframe.copy()
    
    # func_df['dob_hour'] = func_df['Date of birth'].dt.hour
    func_df['dob_dayofweek'] = func_df['Date of birth'].dt.dayofweek
    func_df['dob_quarter'] = func_df['Date of birth'].dt.quarter
    func_df['dob_month'] = func_df['Date of birth'].dt.month
    func_df['dob_year'] = func_df['Date of birth'].dt.year
    func_df['dob_dayofyear'] = func_df['Date of birth'].dt.dayofyear
    func_df['dob_dayofmonth'] = func_df['Date of birth'].dt.day
    func_df['dob_weekofyear'] = func_df['Date of birth'].dt.weekofyear        
        
    func_df = func_df.drop('Date of birth', axis = 1)    
        
    return func_df

def name_the_retained_features(columns, instance):
    
    "takes in a list of columns, and a fitted instance of a feature selector. returns the columns that were selected"
    
    column_variances = instance.get_support() # an array of length columns, True if that column has variance greater than 0
    columns_with_variances = list(zip(columns, column_variances)) 
    retained_columns = [col[0] for col in columns_with_variances if col[1]] # we only want the columns with significant variance

    return retained_columns