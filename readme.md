#### Feature selection

Feature selection is really important. In the real world features correlate with eachother, or they're poorly populated, or they're irrelevant. We need a way of selecting the right ones for our that isn't "oh i reckon it'll be these ones". 

You could try many combinations of features, but what if you have hundreds? there isn't time.

Here I'm outlining a few strategies that sci-kit learn offers to reduce the number of features you're using. 

These are (briefly):

* VarianceThreshold() - just remove the features that have variance lower than x
* Select K-best/percentile - do a univariate analysis, and return the features that have the best coefficients
* RFECV (recursive feature elimination with cross validation) - run model on all features with cross validation, drop the least important features, and run again. Repeat until the desired number of features is reached.
* l1 Penalization
* l2 Penalization
* PCA

##### VarianceThreshold()

removes all features that have a variance less than x. These features are unlikely to be having a large impact fairly

```python
var_threshold = VarianceThreshold() # default variance cutoff is 0 (i.e. the column has all the same values)
x_reduced = pd.DataFrame(var_threshold.fit_transform(x), index = x.index)

column_variances = var_threshold.get_support() # an array of length x.columns, True if that column has variance greater than 0
columns_with_variances = list(zip(x.columns, column_variances)) 
retained_columns = [col[0] for col in columns_with_variances if col[1]] # we only want the columns with significant variance
x_reduced.columns = retained_columns
```

##### Select K best

Calculates the relationship between each feature in x, and y, separately. For example if your df has 4 features, SelectKBest works out the strength of x1 and y, x2 and y, etc.

Next, it will rank your features by the strength of their relationship with y, and strip out the worst features so that you are left with K features

```python
selectbest = SelectKBest(f_regression, k = 10)
k_best_features = pd.DataFrame(selectbest.fit_transform(x, y, index = x.index)
```

##### RFECV

Recursive Feature Elimination with Cross Validation

Trains a supplied model with all features on the supplied target variable, removes the worst performing feature, and goes again until it has as few features as the user asked for.

This is a slight extension of the more basic RFE as it also includes Cross Validation - as we are training and re-training our model we could begin to overfit, cross validation is a means of shuffling our data around so that we're not constantly re-predicting the same data.

```python
l = LogisticRegression(max_iter = 1000) # this is the actual model

# and this is the RFECV wrapper, which will fit and re-fit our model until it's removed enough features
rfecv = RFECV(l, step = 1, 
              min_features_to_select = 10, 
              cv = 5)

# and now we actually apply it to our data
rfecv_df = pd.DataFrame(rfecv.fit_transform(x, y), index = x.index)
```

##### l1 Penalization

The solution to our regression problem is some vector, which contains numbers that we use to weight the columns of x before summing them together. 

l1 penalization forces a lot of these numbers to be 0. Of course, when we multiply that column by 0 and add it, that column adds nothing and has no value.

```python
reg = LogisticRegression(penalty = 'l1',
                         solver = 'liblinear',
                         C = 0.1) # lower value means more regularization

reg.fit(x, y)
```

Note the extra parameter there - C. It indicates how much regularization we're doing.

If a column's coefficient is 0, then that column might as well not exist. We can remove all the columns we don't care about using another wrapper.

```python
# because reg has an l1 penalty, SelectFromModel will automatically strip out all the coefficients smaller than 1e-5
model = SelectFromModel(reg, prefit = True) # prefit tells it that reg has already been fitted
l1_x = pd.DataFrame(model.transform(x), index = x.index) # strips out those columns which model said were no use
```

##### l2 penalization

l2 penalization is a lot like l1 except it doesn't shrink coefficients to 0, it just shrinks them. It works because the model aims to reduce the model's loss, and l2-penalized-models add the slope squared to the loss. Reducing this loss gets much better returns than l1, so it doesn't take as large a reduction in coefficients to achieve a good result.

```python
reg = LogisticRegression(penalty = 'l2',
                         solver = 'liblinear',
                         C = 0.1) # lower value means more regularization

reg.fit(x, y);
# we could use SelectFromModel again to strip out our weaker features, but if we did that we might as well be using l1
```